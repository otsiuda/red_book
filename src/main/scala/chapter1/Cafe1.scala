package chapter1

class Cafe1 {

  // passing of Payments object is some sort of dependency injection
  def buyCoffee(cc: CreditCard1, p: Payments): Coffee1 = {
    val cup = Coffee1(10)
    // side effect still exists, but testability more clear
    p.charge(cc, cup.price)
    cup
  }

}

case class CreditCard1()

case class Coffee1(price: BigDecimal)

class Payments {

  def charge(cc: CreditCard1, amount: BigDecimal): Unit = {
    // connect to payment provider
  }

}
