package chapter1

class Cafe0 {

  def buyCoffee(cc: CreditCard0): Coffee0 = {
    val cup = Coffee0(10)

    // side effect
    cc.charge(cup.price)

    cup
  }

}

case class CreditCard0() {
  def charge(amount: BigDecimal): Unit = {
    // connect to payment provider
  }
}

case class Coffee0(price: BigDecimal)
