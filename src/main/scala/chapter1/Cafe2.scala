package chapter1

class Cafe2 {

  def buyCoffee(cc: CreditCard2): (Coffee2, Charge2) = {
    val cup = Coffee2(10)
    (cup, Charge2(cc, cup.price))
  }

  def buyCoffees(cc: CreditCard2, n: Int): (List[Coffee2], Charge2) = {
    val purchases: List[(Coffee2, Charge2)] = List.fill(n)(buyCoffee(cc))
    val (coffees, charges) = purchases.unzip
    (coffees, charges.reduce((a, b) => a.combine(b)))
  }

}

case class CreditCard2()

case class Coffee2(price: BigDecimal)

case class Charge2(cc: CreditCard2, amount: BigDecimal) {

  def combine(other: Charge2): Charge2 =
    if (other.cc == cc)
      Charge2(cc, amount + other.amount)
    else
      throw new Exception("Can't combine charges to different cards")

}

object Charge2 {

  def coalesce(charges: List[Charge2]): List[Charge2] = {
    charges.groupBy(_.cc).values.map(_.reduce(_.combine(_))).toList
  }

}

